package workpool

import (
	"sync"

	"golang.org/x/sync/errgroup"
)

// WorkPool needed to not DoS API we consume, so we can run up to {limit} of workers to query the API
// simultaneously, in that case we will get our results faster but won't kill the API or face throttling
type WorkPool struct {
	maxWorkers       int
	taskChan         chan func() error
	currentlyWorking int32
	errg             errgroup.Group
}

func NewWorkerPool(maxWorkers int) *WorkPool {
	return &WorkPool{
		maxWorkers: maxWorkers,
		taskChan:   make(chan func() error),
	}
}

// Simple and fast implementation
func (wp *WorkPool) Run(wg *sync.WaitGroup, tasks ...func() error) {
	// TODO: run up to wp.maxWorkers
	// TODO: use atomics to control how many workers run simultaneously
	// TODO: make concurrent
	wg.Add(len(tasks))
	// emulate concurrency
	go func() {
		for _, task := range tasks {
			task := task
			if err := task(); err != nil {
			}
			wg.Done()
		}
	}()

}
