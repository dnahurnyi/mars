package router

import (
	"context"
	"encoding/json"
	"fmt"
	"mars/api"
	"net/http"
	"time"
)

type router struct {
	service        Service
	picturesFilter api.Filter
}

func NewRouter(svc Service, picturesFilter api.Filter) *router {
	return &router{
		service:        svc,
		picturesFilter: picturesFilter,
	}
}

type Service interface {
	Pictures(ctx context.Context, in api.Filter) (map[string][]string, error)
}

func (r *router) ServePictures(w http.ResponseWriter, req *http.Request) {
	ctx := context.Background()
	// TODO: set time as an interface to the router so we can mock it
	filter, err := reqToFilter(r.picturesFilter, req, time.Now())
	if err != nil {
		fmt.Fprintf(w, "parse request to pictures filter: %v\n", err)
		return
	}

	res, err := r.service.Pictures(ctx, filter)
	if err != nil {
		fmt.Fprintf(w, "get pictures: %v\n", err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(res)
}
