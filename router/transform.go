package router

import (
	"fmt"
	"mars/api"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

const (
	limitParam = "limit"
	dateParam  = "fromDate"
	dateLayout = "2006-01-02"
)

func reqToFilter(filter api.Filter, req *http.Request, today time.Time) (api.Filter, error) {
	if req == nil {
		return filter, nil
	}

	m, err := url.ParseQuery(req.URL.RawQuery)
	if err != nil {
		return api.Filter{}, fmt.Errorf("get query params: %w", err)
	}
	if val := m.Get(limitParam); len(val) > 0 {
		limit, err := strconv.Atoi(val)
		if err != nil {
			return api.Filter{}, fmt.Errorf("parse limit param from request: %w", err)
		}
		filter.ResultsPerDay = limit
	}

	if val := m.Get(dateParam); len(val) > 0 {
		dates, err := fromDateParamToDates(val, today)
		if err != nil {
			return api.Filter{}, fmt.Errorf("parse fromDate param from request: %w", err)
		}
		filter.Dates = dates
	}

	return filter, nil
}

func fromDateParamToDates(in string, today time.Time) ([]string, error) {
	fromDate, err := time.Parse(dateLayout, in)
	if err != nil {
		return nil, fmt.Errorf("bad data format: %w", err)
	}
	if fromDate.After(today) {
		return nil, fmt.Errorf("provided date is ahead of now")
	}
	res := []string{fromDate.Format(dateLayout)}
	for {
		fromDate = fromDate.Add(time.Hour * 24)
		if fromDate.After(today) {
			break
		}
		res = append(res, fromDate.Format(dateLayout))
	}
	return res, nil
}
