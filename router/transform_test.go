package router

import (
	"fmt"
	"mars/api"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func Test_fromDateParamToDates(t *testing.T) {
	tests := []struct {
		name   string
		in     string
		today  time.Time
		want   []string
		errMsg string
	}{
		{
			name:  "5-days",
			in:    "2022-01-06",
			today: time.Date(2022, 1, 10, 0, 0, 0, 0, time.UTC),
			want: []string{
				"2022-01-06", "2022-01-07", "2022-01-08", "2022-01-09", "2022-01-10",
			},
		},
		{
			name:  "1-day",
			in:    "2022-01-10",
			today: time.Date(2022, 1, 10, 0, 0, 0, 0, time.UTC),
			want: []string{
				"2022-01-10",
			},
		},
		{
			name:   "ahead-of-time-error",
			in:     "2032-01-10",
			today:  time.Date(2022, 1, 10, 0, 0, 0, 0, time.UTC),
			want:   nil,
			errMsg: fmt.Errorf("provided date is ahead of now").Error(),
		},
		{
			name:   "bad-data-format",
			in:     "20-32-1-10",
			today:  time.Date(2022, 1, 10, 0, 0, 0, 0, time.UTC),
			want:   nil,
			errMsg: `bad data format: parsing time "20-32-1-10" as "2006-01-02": cannot parse "2-1-10" as "2006"`,
		},
	}

	for _, tc := range tests {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()
			got, err := fromDateParamToDates(tc.in, tc.today)
			if tc.errMsg != "" {
				require.Equal(t, tc.errMsg, err.Error())
			}
			require.Equal(t, tc.want, got)
		})
	}
}

func Test_reqToFilter(t *testing.T) {

	tests := []struct {
		name     string
		defaultF api.Filter
		getReq   func() *http.Request
		today    time.Time
		want     api.Filter
		errMsg   string
	}{
		{
			name: "get-data-from-request",
			defaultF: api.Filter{
				Dates:         []string{},
				ResultsPerDay: 25,
			},
			getReq: func() *http.Request {
				req, _ := http.NewRequest(http.MethodGet, "somequery.com/photos?limit=20&fromDate=2022-04-05", nil)
				return req
			},
			today: time.Date(2022, 4, 7, 0, 0, 0, 0, time.UTC),
			want: api.Filter{
				ResultsPerDay: 20,
				Dates: []string{
					"2022-04-05", "2022-04-06", "2022-04-07",
				},
			},
		},
		{
			name: "no-data-in-request",
			defaultF: api.Filter{
				Dates:         []string{"2022-04-11"},
				ResultsPerDay: 25,
			},
			getReq: func() *http.Request {
				req, _ := http.NewRequest(http.MethodGet, "somequery.com/photos", nil)
				return req
			},
			today: time.Date(2022, 4, 7, 0, 0, 0, 0, time.UTC),
			want: api.Filter{
				Dates:         []string{"2022-04-11"},
				ResultsPerDay: 25,
			},
		},
		{
			name: "bad-limit",
			defaultF: api.Filter{
				Dates:         []string{"2022-04-11"},
				ResultsPerDay: 25,
			},
			getReq: func() *http.Request {
				req, _ := http.NewRequest(http.MethodGet, "somequery.com/photos?limit=bad", nil)
				return req
			},
			today:  time.Date(2022, 4, 7, 0, 0, 0, 0, time.UTC),
			want:   api.Filter{},
			errMsg: `parse limit param from request: strconv.Atoi: parsing "bad": invalid syntax`,
		},
	}

	for _, tc := range tests {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()
			got, err := reqToFilter(tc.defaultF, tc.getReq(), tc.today)
			if tc.errMsg != "" {
				require.Equal(t, tc.errMsg, err.Error())
			}
			require.Equal(t, tc.want, got)
		})
	}
}
