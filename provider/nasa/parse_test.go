package nasa

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_photosToUrls(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name  string
		in    []photo
		limit int
		want  []string
	}{
		{
			name: "equal-to-limit-pictures",
			in: []photo{
				{ImgSource: "pic#1"},
				{ImgSource: "pic#2"},
				{ImgSource: "pic#3"},
			},
			limit: 3,
			want: []string{
				"pic#1", "pic#2", "pic#3",
			},
		},
		{
			name: "more-than-limit-pictures",
			in: []photo{
				{ImgSource: "pic#1"},
				{ImgSource: "pic#2"},
				{ImgSource: "pic#3"},
				{ImgSource: "pic#4"},
			},
			limit: 3,
			want: []string{
				"pic#1", "pic#2", "pic#3",
			},
		},
		{
			name: "lower-than-limit-pictures",
			in: []photo{
				{ImgSource: "pic#1"},
				{ImgSource: "pic#2"},
			},
			limit: 3,
			want: []string{
				"pic#1", "pic#2",
			},
		},
		{
			name:  "no-pictures",
			in:    []photo{},
			limit: 3,
			want:  nil,
		},
	}
	for _, tc := range tests {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()
			got := photosToUrls(tc.in, tc.limit)
			require.Equal(t, tc.want, got)
		})
	}
}
