package nasa

type response struct {
	Photos []photo `json:"photos"`
}

type photo struct {
	ID        int    `json:"id"`
	Sol       int    `json:"sol"`
	Camera    camera `json:"camera"`
	ImgSource string `json:"img_src"`
	EarthDate string `json:"earth_date"`
	Rover     rover  `json:"rover"`
}

type camera struct {
	ID       int    `json:"id"`
	Name     string `json:"name"`
	RoverID  int    `json:"rover_id"`
	FullName string `json:"full_name"`
}

type rover struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	LandingDate string `json:"landing_date"`
	LaunchDate  string `json:"launch_date"`
	Status      string `json:"status"`
}

func photosToUrls(in []photo, limit int) []string {
	if len(in) == 0 {
		return nil
	}
	if len(in) >= limit {
		in = in[:limit]
	}
	res := make([]string, len(in))
	for i, photo := range in {
		res[i] = photo.ImgSource
	}
	return res
}
