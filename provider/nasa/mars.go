package nasa

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"mars/api"
	"mars/workpool"
	"net/http"
	"sync"
)

type Provider struct {
	url        string
	apiKey     string
	maxWorkers int
}

func NewProvider(url string, apiKey string, maxWorkers int) *Provider {
	return &Provider{
		url:        url,
		apiKey:     apiKey,
		maxWorkers: maxWorkers,
	}
}

func (p *Provider) GetPictures(ctx context.Context, filter api.Filter) (map[string][]string, error) {
	res := make(map[string][]string)
	m := &sync.Mutex{}
	wp := workpool.NewWorkerPool(p.maxWorkers)
	tasks := make([]func() error, len(filter.Dates))
	for i, date := range filter.Dates {
		date := date
		tasks[i] = func() error {
			// assume that one query can return any amount of results up to a limit,
			// otherwise we would create list of queries to get required amount if pics for one date
			pics, err := p.query(date, filter)
			if err != nil {
				return fmt.Errorf("call query to get pictures: %w", err)
			}
			m.Lock()
			res[date] = append(res[date], pics...)
			m.Unlock()
			return nil
		}
	}
	wg := &sync.WaitGroup{}
	wp.Run(wg, tasks...)
	wg.Wait()

	return res, nil
}

func filterToQueryParams(req *http.Request, apiKey, date string) string {
	q := req.URL.Query()
	q.Add("api_key", apiKey)
	q.Add("earth_date", date)
	// use new fields from filter here
	return q.Encode()
}

func (p *Provider) query(date string, filter api.Filter) ([]string, error) {
	req, err := http.NewRequest(http.MethodGet, p.url, nil)
	if err != nil {
		return nil, fmt.Errorf("create http request: %w", err)
	}
	req.URL.RawQuery = filterToQueryParams(req, p.apiKey, date)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("call http request: %w", err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("read http response body: %w", err)
	}

	var resData response
	err = json.Unmarshal(body, &resData)
	if err != nil {
		return nil, fmt.Errorf("can't parse provider reponse into data: %w", err)
	}
	if len(resData.Photos) == 0 {
		return nil, nil
	}

	return photosToUrls(resData.Photos, filter.ResultsPerDay), nil
}
