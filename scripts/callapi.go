package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

func main() {
	// TODO: receive params from os.Args or the whole URL
	if len(os.Args) < 2 {
		fmt.Println("please pass the url to call in the script")
		return
	}

	response, err := http.Get(os.Args[1])

	if err != nil {
		fmt.Print(err.Error())
		os.Exit(1)
	}

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(string(responseData))
}
