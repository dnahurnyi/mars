package main

import (
	"fmt"
	"mars/api"
	"mars/cache/simple"
	"mars/provider/nasa"
	"mars/router"
	"net/http"
)

func main() {
	defer func() {
		// TODO: graceful shutdown
	}()
	cache := simple.NewCache()
	provider := nasa.NewProvider(
		// TODO: get those from env
		"https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos",
		"MXvARmEdIFcblJBcvwhrdG3gFuAZUnQ1cfSWUhBk",
		10,
	)
	svc := api.NewService(cache, provider)

	r := router.NewRouter(svc, api.Filter{
		// TODO: get those from env
		Dates:         []string{"2022-1-2"},
		ResultsPerDay: 3,
	})

	http.HandleFunc("/pictures", r.ServePictures)
	// TODO: use logs instead of printlns
	fmt.Println("server started")
	http.ListenAndServe(":8090", nil)
}
