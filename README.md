# TODO
Things to do (except those mentioned in the // TODO comments in the code):

	- dockerize
	- implement real worker pool
	- use better cache implementations or standalone caches like redis
	- system tests
	- panic recover in main
	- metrices
	- healthcheck
	- throttling

# How to use
Start API with command:
```
	make run-api
```

Start script that call API with default params:
```
	make run-script
```
