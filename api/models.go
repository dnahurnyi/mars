package api

// Filter is the model that can be extended for new requirements like picture source or any other
type Filter struct {
	Dates         []string
	ResultsPerDay int
}
