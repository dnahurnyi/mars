package api

import (
	"context"
	"fmt"
)

func (s *service) Pictures(ctx context.Context, filter Filter) (map[string][]string, error) {
	cacheData, err := s.cache.GetPictures(filter)
	if err != nil {
		return nil, fmt.Errorf("get data from cache: %w", err)
	}
	incompleteFilter := dataToIncompleteFilter(cacheData, filter)
	if incompleteFilter == nil {
		return cacheData, nil
	}
	providerData, err := s.provider.GetPictures(ctx, *incompleteFilter)
	if err != nil {
		return nil, fmt.Errorf("get data from data provider: %w", err)
	}
	err = s.cache.PutPictures(providerData)
	if err != nil {
		return nil, fmt.Errorf("save new data in cache: %w", err)
	}

	return mergeData(providerData, cacheData), nil
}
