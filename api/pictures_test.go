package api_test

import (
	"context"
	"fmt"
	"mars/api"
	"mars/api/mock"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
)

func Test_Pictures(t *testing.T) {
	t.Parallel()

	ctx := context.Background()
	data := map[string][]string{
		"date#1": {"pic#1.1", "pic#1.2"},
		"date#2": {"pic#2.1", "pic#2.2", "pic#2.3"},
		"date#3": {"pic#3.1", "pic#3.2", "pic#3.3"},
	}

	t.Run("cache-is-empty", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		cache := mock.NewMockCache(ctrl)
		dp := mock.NewMockDataProvider(ctrl)
		filter := api.Filter{
			Dates:         []string{"date#1", "date#2"},
			ResultsPerDay: 3,
		}
		svc := api.NewService(cache, dp)

		cache.EXPECT().
			GetPictures(filter).
			Return(map[string][]string{}, nil)

		dp.EXPECT().
			GetPictures(ctx, filter).
			Return(data, nil)

		cache.EXPECT().
			PutPictures(data).
			Return(nil)

		res, err := svc.Pictures(ctx, filter)
		require.Nil(t, err)
		require.Equal(t, data, res)
	})

	t.Run("cache-has-some-data", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		cache := mock.NewMockCache(ctrl)
		dp := mock.NewMockDataProvider(ctrl)
		filter := api.Filter{
			Dates:         []string{"date#1", "date#2"},
			ResultsPerDay: 3,
		}
		svc := api.NewService(cache, dp)

		cache.EXPECT().
			GetPictures(filter).
			Return(map[string][]string{
				"date#1": {"pic#1.1", "pic#1.2"},
				"date#2": {"pic#2.1", "pic#2.2", "pic#2.3"},
			}, nil)

		providerData := map[string][]string{
			"date#1": {"pic#1.1", "pic#1.2", "pic#1.3"},
		}

		dp.EXPECT().
			GetPictures(ctx, api.Filter{
				Dates:         []string{"date#1"},
				ResultsPerDay: 3,
			}).
			Return(providerData, nil)

		cache.EXPECT().
			PutPictures(providerData).
			Return(nil)

		res, err := svc.Pictures(ctx, filter)
		require.Nil(t, err)
		require.Equal(t, map[string][]string{
			"date#1": {"pic#1.1", "pic#1.2", "pic#1.3"},
			"date#2": {"pic#2.1", "pic#2.2", "pic#2.3"},
		}, res)
	})

	t.Run("cache-has-all-data", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		cache := mock.NewMockCache(ctrl)
		dp := mock.NewMockDataProvider(ctrl)
		filter := api.Filter{
			Dates:         []string{"date#1", "date#2"},
			ResultsPerDay: 3,
		}
		svc := api.NewService(cache, dp)

		data := map[string][]string{
			"date#1": {"pic#1.1", "pic#1.2", "pic#1.3"},
			"date#2": {"pic#2.1", "pic#2.2", "pic#2.3"},
		}

		cache.EXPECT().
			GetPictures(filter).
			Return(data, nil)

		res, err := svc.Pictures(ctx, filter)
		require.Nil(t, err)
		require.Equal(t, data, res)
	})

	t.Run("cache-get-error", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		cache := mock.NewMockCache(ctrl)
		dp := mock.NewMockDataProvider(ctrl)
		filter := api.Filter{
			Dates:         []string{"date#1", "date#2"},
			ResultsPerDay: 3,
		}
		svc := api.NewService(cache, dp)

		cache.EXPECT().
			GetPictures(filter).
			Return(nil, fmt.Errorf("internal error"))

		res, err := svc.Pictures(ctx, filter)
		require.Nil(t, res)
		require.Equal(t, fmt.Errorf("get data from cache: %w", fmt.Errorf("internal error")), err)
	})

	t.Run("provider-get-error", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		cache := mock.NewMockCache(ctrl)
		dp := mock.NewMockDataProvider(ctrl)
		filter := api.Filter{
			Dates:         []string{"date#1", "date#2"},
			ResultsPerDay: 3,
		}
		svc := api.NewService(cache, dp)

		cache.EXPECT().
			GetPictures(filter).
			Return(nil, nil)

		dp.EXPECT().
			GetPictures(ctx, api.Filter{
				Dates:         []string{"date#1", "date#2"},
				ResultsPerDay: 3,
			}).
			Return(nil, fmt.Errorf("internal error"))

		res, err := svc.Pictures(ctx, filter)
		require.Nil(t, res)
		require.Equal(t, fmt.Errorf("get data from data provider: %w", fmt.Errorf("internal error")), err)
	})

	t.Run("cache-put-error", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		cache := mock.NewMockCache(ctrl)
		dp := mock.NewMockDataProvider(ctrl)

		svc := api.NewService(cache, dp)

		filter := api.Filter{
			Dates:         []string{"date#1", "date#2"},
			ResultsPerDay: 3,
		}
		cache.EXPECT().
			GetPictures(filter).
			Return(nil, nil)

		dp.EXPECT().
			GetPictures(ctx, filter).
			Return(map[string][]string{
				"date#1": {"pic#1.1", "pic#1.2", "pic#1.3"},
				"date#2": {"pic#2.1", "pic#2.2", "pic#2.3"},
			}, nil)

		cache.EXPECT().
			PutPictures(map[string][]string{
				"date#1": {"pic#1.1", "pic#1.2", "pic#1.3"},
				"date#2": {"pic#2.1", "pic#2.2", "pic#2.3"},
			}).
			Return(fmt.Errorf("internal error"))

		res, err := svc.Pictures(ctx, filter)
		require.Nil(t, res)
		require.Equal(t, fmt.Errorf("save new data in cache: %w", fmt.Errorf("internal error")), err)
	})
}
