package api

// TODO: test it
func dataToIncompleteFilter(data map[string][]string, filter Filter) *Filter {
	incompDates := make([]string, 0)
	for _, date := range filter.Dates {
		if pics, ok := data[date]; !ok || len(pics) < filter.ResultsPerDay {
			incompDates = append(incompDates, date)
		}
	}
	if len(incompDates) == 0 {
		return nil
	}
	return &Filter{
		Dates:         incompDates,
		ResultsPerDay: filter.ResultsPerDay,
	}
}

// TODO: test it
func mergeData(providerData, cacheData map[string][]string) map[string][]string {
	if providerData == nil {
		return cacheData
	}

	for date, pics := range providerData {
		cacheData[date] = pics
	}
	return cacheData
}
