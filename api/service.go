package api

import "context"

type service struct {
	cache         Cache
	provider      DataProvider
	defaultFilter Filter
}

func NewService(
	cache Cache,
	provider DataProvider,
) *service {
	return &service{
		cache:    cache,
		provider: provider,
	}
}

type Cache interface {
	PutPictures(datePics map[string][]string) error
	GetPictures(filter Filter) (map[string][]string, error)
}

type DataProvider interface {
	GetPictures(ctx context.Context, filter Filter) (map[string][]string, error)
}
