package simple

import "mars/api"

type Cache struct {
	storage map[string][]string
}

func NewCache() *Cache {
	return &Cache{
		storage: make(map[string][]string),
	}
}

func (c *Cache) GetPictures(filter api.Filter) (map[string][]string, error) {
	res := make(map[string][]string)
	for _, date := range filter.Dates {
		if pics, ok := c.storage[date]; ok {
			res[date] = limitResults(pics, filter.ResultsPerDay)
		}
	}

	return res, nil
}

func limitResults(in []string, limit int) []string {
	if len(in) >= limit {
		return in[:limit]
	}
	return in
}

func (c *Cache) PutPictures(datePics map[string][]string) error {
	for date, pics := range datePics {
		// overwrite what we have since we don't expect new data to be an update
		c.storage[date] = pics
	}
	return nil
}
