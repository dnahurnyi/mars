package simple_test

import (
	"mars/api"
	"mars/cache/simple"
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_Cache(t *testing.T) {
	t.Parallel()

	t.Run("no-results-from-empty-cache", func(t *testing.T) {
		t.Parallel()
		cache := simple.NewCache()

		res, err := cache.GetPictures(api.Filter{
			Dates:         []string{"date#1"},
			ResultsPerDay: 2,
		})
		require.Nil(t, err)
		require.Equal(t, map[string][]string{}, res)
	})

	t.Run("full-miss", func(t *testing.T) {
		t.Parallel()
		cache := simple.NewCache()
		err := cache.PutPictures(map[string][]string{
			"date#2": {"pic#1", "pic#2"},
		})
		require.Nil(t, err)
		res, err := cache.GetPictures(api.Filter{
			Dates:         []string{"date#1"},
			ResultsPerDay: 2,
		})
		require.Nil(t, err)
		require.Equal(t, map[string][]string{}, res)
	})

	t.Run("partial-results", func(t *testing.T) {
		t.Parallel()
		cache := simple.NewCache()
		err := cache.PutPictures(map[string][]string{
			"date#2": {"pic#2"},
			"date#1": {"pic#1", "pic#2"},
		})
		require.Nil(t, err)
		res, err := cache.GetPictures(api.Filter{
			Dates:         []string{"date#1"},
			ResultsPerDay: 2,
		})
		require.Nil(t, err)
		require.Equal(t, map[string][]string{
			"date#1": {"pic#1", "pic#2"},
		}, res)
	})

	t.Run("limit", func(t *testing.T) {
		t.Parallel()
		cache := simple.NewCache()
		err := cache.PutPictures(map[string][]string{
			"date#2": {"pic#2"},
			"date#1": {"pic#1", "pic#2"},
		})
		require.Nil(t, err)
		res, err := cache.GetPictures(api.Filter{
			Dates:         []string{"date#1"},
			ResultsPerDay: 1,
		})
		require.Nil(t, err)
		require.Equal(t, map[string][]string{
			"date#1": {"pic#1"},
		}, res)
	})
}
