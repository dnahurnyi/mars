.PHONY: mocks
mocks:
	mockgen -source=api/service.go -destination=api/mock/service.go -package=mock

.PHONY: run-api
run-api:
	go run main.go

.PHONY: run-script
run-script:
	go run scripts/callapi.go "http://localhost:8090/pictures?limit=10&fromDate=2022-03-30"